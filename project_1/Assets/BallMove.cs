﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    int BallJumpPower = 5000;
    int BallMovePower = 50;
    bool IsGrounded;

    Rigidbody Ball;

    public int BallMovePower1
    {
        get
        {
            return BallMovePower;
        }

        set
        {
            BallMovePower = value;
        }
    }

    void Start()
    {
        Ball = this.gameObject.GetComponent<Rigidbody>();
        Ball.mass = 10;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallJumpPower);

        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.forward * BallJumpPower);


        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallJumpPower);


        if (Input.GetKey(KeyCode.A))
            Ball.AddForce(Vector3.left * BallJumpPower);


        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
            Ball.AddForce(Vector3.up * BallJumpPower);


    }

    private void OnCollisionEnter(Collision collision)
    {
        IsGrounded = true;

    }


    void OnCollisionExit(Collision collision)
    {
        IsGrounded = false;

    }
}

